import java.util.Scanner;
public class LoginPassword
{
    public static boolean verifyLogin(String[][] credentials, 
    String user, String pass)
    {
        boolean correct = false;
        
        for (int i = 0; i < credentials.length; i++)
        {
            if( credentials[i][0].equals(user) 
                && credentials[i][1].equals(pass))
                correct = true;
        }
        
        return correct;
    }
    
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int count = 3;
        String user, pass;
        boolean correct = false;
        
        String[][] credentials = new String[3][2];
        credentials[0][0] = "nacho";
        credentials[0][1] = "1234";
        credentials[1][0] = "pablo";
        credentials[1][1] = "5678";
        credentials[2][0] = "arturo";
        credentials[2][1] = "9012";
        
        while(count-- != 0 && !correct)
        {
            System.out.printf("Enter you login: ");
            user = sc.nextLine();
            System.out.printf("Enter you password: ");
            pass = sc.nextLine();
            if(verifyLogin(credentials, user, pass))
            {
                System.out.printf("Welcome %s!\n", user);
                correct = true;
            }
            if(!correct)
                System.out.printf("Wrong credentials. Try again\n\n");
            
        }
        if(!correct)
            System.out.printf("Numer of tries exceeded!\n");
    }
}
