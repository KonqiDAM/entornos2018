import java.util.Scanner;
public class CircleArea
{
    public static final double PI = 3.14159;

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.printf("Enter the radius: ");
        int radius = sc.nextInt();
        System.out.printf("The Area is: %.2f", PI * radius * radius);
    }
}

