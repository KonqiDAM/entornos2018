
import java.io.*;
import java.util.*;


public class Main2 {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        PrintWriter printWriter = null;

        ArrayList<String> s = new ArrayList<>();
        do {
            System.out.println("Enter a text until white space");
            s.add(sc.nextLine());
        } while (!s.get(s.size() - 1).equals(""));

        try {
            printWriter = new PrintWriter("sentences.txt");
            for (int i = 0; i < s.size(); i++) {
                printWriter.println(s.get(i));

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

}
