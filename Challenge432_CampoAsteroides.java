import java.util.Scanner;

public class Challenge432_CampoAsteroides
{
    public static boolean solveMap(char[][] map, int row, int col) 
    {
        if (map[row][col] == 'F')
        {
            return true;
        }
        else
        {
            map[row][col] = '*';
            return  ( row > 0 && map[row-1][col] != '*' ? 
                    solveMap(map, row-1, col) : false )
                || ( row < map.length-1 && map[row+1][col] != '*'  ? 
                    solveMap(map, row+1, col) : false )
                || ( col > 0 && map[row][col-1] != '*'  ? 
                    solveMap(map, row, col-1) : false )
                || ( col < map[0].length-1 && map[row][col+1] != '*' ? 
                    solveMap(map, row, col+1) : false);
        }
    }
    
    
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        int rows, cols;
        int row = 0, col = 0;
        
        while (sc.hasNextLine())
        {
            rows = sc.nextInt();
            cols = sc.nextInt();
            sc.nextLine();
            
            char[][] map = new char[rows][cols];
            
            for (int i = 0; i < rows; i++)
            {
                String line = sc.nextLine();
                for (int j = 0; j < cols; j++)
                {
                    map[i][j] = line.charAt(j);
                    if(map[i][j] == 'S')
                    {
                        row = i;
                        col = j;
                    }
                }
            }
                if(solveMap(map, row, col))
                    System.out.println("SI");
                else
                    System.out.println("NO");
        } 
    }
}
