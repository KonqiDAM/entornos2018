import java.util.Scanner;

class Companies
{
    protected String name;
    protected short year;
    
    public Companies()
    {
        name = "";
        year = -1;
    }
    
    public Companies(Companies c)
    {
        name = c.name;
        year = c.year;
    }
    
    public Companies(String n, short y)
    {
        setName(n);
        setYear(y);
    }
    
    public void setName(String n) { name = n; }
    public void setYear(short n) { year = n; }
    
    public String getName() { return name; }
    public short getYear() { return year; }
}

class VideoGame
{
    
    private String title;
    private String genre;
    private float price;
    private Companies company;
    
    public VideoGame (String newTitle, String newGenere, float newPrice, Companies c)
    {
        setTitle(newTitle);
        setGenere(newGenere);
        setPrice(newPrice);
        company = new Companies(c);
    }
    
    public VideoGame()
    {
        setTitle("");
        setGenere("");
        setPrice(0);
        company = new Companies();
    }
    
    public void setTitle(String newTitle) { title = newTitle; }
    public void setGenere(String newGenere) { genre = newGenere; }
    public void setPrice(float newPrice) { price = newPrice; }
    public void setCompany(Companies n) { company = n; }
    
    public String getTitle() { return title; }
    public String getGenere() { return genre; }
    public float getPrice() { return price; }
    public Companies getCompany() { return company; }
    
    
}


public class VideoGameList2
{
    static final int NUMGAMES = 5;
    
    public static void main(String[] args)
    {
        int cheapest = 0, expensive = 0;
        Scanner sc = new Scanner(System.in);
        VideoGame[] games = new VideoGame[NUMGAMES];
        Companies c = new Companies();
        for (int i = 0; i < NUMGAMES; i++)
        {
            games[i] = new VideoGame();
            System.out.printf("Game N%d title: ", i+1);
            games[i].setTitle(sc.nextLine());
            System.out.printf("Game N%d genre: ", i+1);
            games[i].setGenere(sc.nextLine());
            System.out.printf("Game N%d price: ", i+1);
            games[i].setPrice(sc.nextFloat());
            sc.nextLine();
            
            System.out.printf("Company name: ");
            c.setName(sc.nextLine());
            System.out.printf("Company year: ");
            c.setYear((short) sc.nextInt());
            sc.nextLine();
            
            games[i].setCompany(c);
            
            if(games[cheapest].getPrice() > games[i].getPrice())
                cheapest = i;
            if(games[cheapest].getPrice() < games[i].getPrice())
                expensive = i;
        }
        
        
        System.out.printf("\nExpensive: %s\n", games[expensive].getTitle());
        System.out.printf("\nChepeast: %s\n", games[cheapest].getTitle());
    }
}
