#include <iostream>
#include <cstdio>
using namespace std;
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

struct bi
{
    int corona;
    int plato;
    float desarollo;
    
};
int main()
{
    int coronas, platos;
    
    scanint(platos);
    scanint(coronas);
    while(platos != 0)
    {
        int total = coronas*platos;
        bi b[total];
        int p[platos];
        int c[coronas];
        for (int i = 0; i < platos; i++)
        {
            scanint(p[i]);
        }
        for (int i = 0; i < coronas; i++)
        {
            scanint(c[i]);
        }
        
        for (int j = 0, i = 0; j < platos; j++)
        {
            for (int k = 0; k < coronas; k++, i++)
            {
                b[i].corona = c[k];
                b[i].plato = p[j];
                b[i].desarollo = (float)p[j]/(float)c[k];
            }
        }
        
        
        bi swap;
        for (int i = 1; i < total; i++)
        {
            int j = i-1;
            while ((j >= 0) && (b[j].desarollo > b[j+1].desarollo))
            {
                swap = b[j];
                b[j] = b[j+1];
                b[j+1] = swap;
                j--;
            }
        }
        
        
        for (int i = 0; i < total-1; i++)
        {
            printf("%d-%d ", b[i].plato, b[i].corona);
        }
        printf("%d-%d\n", b[total-1].plato, b[total-1].corona);
        
        scanint(platos);
        scanint(coronas);
    }
    return 0x0;
}
