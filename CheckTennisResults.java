import java.util.Scanner;
public class CheckTennisResults
{
    public static final int N_MATCHES = 3;
    
    public static boolean validateGame(String game) 
                            throws IllegalArgumentException
    {
        boolean correct = true;
        int min, max;
        min = Integer.parseInt(game.charAt(0) + "");
        max = Integer.parseInt(game.charAt(2) + "");
        
        if(min > max)
        {
            min += max;
            max = min - max;
            min -= max;
        }
        
        if( max != 6)
            correct = false;
        else if(max-min < 2)
            correct = false;
        
        return correct;
    }
    
    public static void main(String[] args)
    {
        
        Scanner sc = new Scanner(System.in);
        String matches[][] = new String[N_MATCHES][];
        String aux;
        
        for (int i = 0; i < N_MATCHES; i++)
        {
            System.out.printf("Enter the result of match number %d: ", i + 1);
            matches[i] = sc.nextLine().split(" ");
        }
        for (int i = 0; i < N_MATCHES; i++)
        {
            boolean allCorrect = true;
            for (int j = 0; j < matches[i].length; j++)
            {
                try
                {
                    if(!validateGame(matches[i][j]))
                        allCorrect = false;
                } catch (IllegalArgumentException e) {
                    allCorrect = false;
                }
                
            }
            if(allCorrect)
                    System.out.printf("Match %d is VALID\n", i + 1);
                else
                    System.out.printf("Match %d is NOT VALID\n", i + 1);
            
        }
    }
}
