/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2sentences;

import java.io.*;
import java.util.*;

/**
 *
 * @author void
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        PrintWriter printWriter = null;
        System.out.println("Enter a sentence");
        String sentence1 = sc.nextLine();
        System.out.println("Enter another sentence");
        String sentence2 = sc.nextLine();
                
        
        try {
            printWriter = new PrintWriter("twoSentences.txt");
            printWriter.println(sentence1);
            printWriter.println(sentence2);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

}
