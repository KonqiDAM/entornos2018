//Ivan Lazcano Sindin
#include <stdio.h>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int a, b = 0, peso;
    scanint(peso);
    while(peso > 0)
    {
        scanint(a);
        peso -= a;
        if(a > 0 && peso > 0)
            b++;
        else
        {
            if(peso == 0)
                b++;
            fprintf(stdout, "%d\n", b);
            if(a != 0)
            {
                do
                {
                    scanint(peso);
                }while(peso);
            }
            b = 0;
            scanint(peso);
        }
        
    }
    
}
