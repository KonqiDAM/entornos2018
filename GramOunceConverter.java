import java.util.Scanner;
public class GramOunceConverter
{
    public static final double OUNCE = 0.03527396;
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        
        
        System.out.printf("Enter grams to convert: ");
        int grams = sc.nextInt();
        
        System.out.printf("The equivalence in ounces is: %.9f", grams*OUNCE);
    }
}
