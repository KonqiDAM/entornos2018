import java.util.Scanner;

class VideoGame
{
    
    private String title;
    private String genre;
    private float price;
    
    public VideoGame (String newTitle, String newGenere, float newPrice)
    {
        setTitle(newTitle);
        setGenere(newGenere);
        setPrice(newPrice);
    }
    
    public VideoGame()
    {
        setTitle("");
        setGenere("");
        setPrice(0);
    }
    
    public void setTitle(String newTitle) { title = newTitle; }
    public void setGenere(String newGenere) { genre = newGenere; }
    public void setPrice(float newPrice) { price = newPrice; }
    
    public String getTitle() { return title; }
    public String getGenere() { return genre; }
    public float getPrice() { return price; }
    
}


public class VideoGameList
{
    static final int NUMGAMES = 5;
    
    public static void main(String[] args)
    {
        int cheapest = 0, expensive = 0;
        Scanner sc = new Scanner(System.in);
        VideoGame[] games = new VideoGame[NUMGAMES];
        
        for (int i = 0; i < NUMGAMES; i++)
        {
            games[i] = new VideoGame();
            System.out.printf("Game N%d title: ", i+1);
            games[i].setTitle(sc.nextLine());
            System.out.printf("Game N%d genre: ", i+1);
            games[i].setGenere(sc.nextLine());
            System.out.printf("Game N%d price: ", i+1);
            games[i].setPrice(sc.nextFloat());
            sc.nextLine();
            if(games[cheapest].getPrice() > games[i].getPrice())
                cheapest = i;
            if(games[cheapest].getPrice() < games[i].getPrice())
                expensive = i;
        }
        System.out.printf("\nExpensive: %s\n", games[expensive].getTitle());
        System.out.printf("\nChepeast: %s\n", games[cheapest].getTitle());
    }
    
}
