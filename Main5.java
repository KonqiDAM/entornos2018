
import java.io.*;
import java.util.*;
import java.time.*;

public class Main5 {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        PrintWriter printWriter = null;

        System.out.println("Width?");
        int width = sc.nextInt();
        System.out.println("Height");
        int height = sc.nextInt();

        
        
        try {
            printWriter = new PrintWriter ("Rectangle.txt");
            for (int i = 0; i < height; i++) {
                for (int k = 0; k < width; k++) {
                    printWriter.print("*");
                }
                printWriter.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

}
