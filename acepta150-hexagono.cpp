//Ivan Lazcano Sindin
#include <iostream>
using namespace std;

int main()
{
    char caracter = 'x';
    int lado = 1;
    int chars, spaces;
    while(caracter != '0' || lado != 0)
    {
        cin >> lado >> caracter;
        if(caracter == '0' && lado == 0)
            return 0;
        else if(lado == 0)
            continue;
        spaces = lado -1;
        chars = lado;
        
        while(spaces > 0)
        {
            for(int i = 0; i < spaces; i++)
            {
                printf(" ");
            }
            for(int i = 0; i < chars; i++)
            {
                printf("%c", caracter);
            }
            spaces--;
            (++chars)++;
            printf("\n");
        }
        while(chars >= lado)
        {
            
            for(int j = 0; j < spaces; j++)
            {
                printf(" ");
            }
            for(int j = 0; j < chars; j++)
            {
                printf("%c", caracter);
            }
            spaces++;
            (--chars)--;
            printf("\n");
            
        }
    }
}
