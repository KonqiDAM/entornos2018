import java.util.Scanner;
public class Triangle
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int total = 0;
        while(num != 0)
        {
            total = 0;
            for (int i = 1; i <= num; i++)
            {
                total += 3*i;
            }
            System.out.println(total);
            num = sc.nextInt();
        }
        
    }
}
