
import java.io.*;
import java.util.*;
import java.time.*;

public class Main7 {

    public static void main(String[] args) {

        Hashtable<Integer,String> days = new Hashtable<Integer, String>() {};
        days.put(1, "Monday");
        days.put(2, "Tuesday");
        days.put(3, "Wednesday");
        days.put(4, "Thursday");
        days.put(5, "Friday");
        days.put(6, "Saturday");
        days.put(7, "Sunday");

        
        Scanner sc = new Scanner(System.in);

        PrintWriter printWriter = null;

        System.out.println("Days of the month?");
        int totalDays = sc.nextInt();
        System.out.println("Month name?");
        sc.nextLine();
        String monthname = sc.nextLine();
        System.out.println("First day of the month");
        int firstDay = sc.nextInt();

        try {
            int actualDayOfWeek = firstDay;
            printWriter = new PrintWriter(monthname +"Calendat.txt");
            printWriter.println(monthname);
            printWriter.println("mon tue wed thu fri sat sun");
            for (int i = 1; i < firstDay; i++) {
                printWriter.print("    ");
            }
            
            for (int i = 1; i <= totalDays; i++) {
                if(actualDayOfWeek > 7)
                {
                    actualDayOfWeek = 1;
                    printWriter.println();
                }
                printWriter.print( (i < 10 ? " "+i : i) + "  ");
                actualDayOfWeek++;
            }
            

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

}
