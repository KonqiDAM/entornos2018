import java.util.Scanner;
public class CountString
{
    public static boolean CountOcurrences(String text, String search, int n)
    {
        boolean found = false;
        for (int i = 0; i < text.length(); i++)
        {
            if(text.contains(search))
            {
                
                if(--n == 0)
                    found = true;
                text = text.substring(0, text.indexOf(search)) + 
                text.substring(text.indexOf(search)+search.length());
                //System.out.println(text);
            }
        }
        
        return found;
    }

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.println(CountOcurrences("This string is just a sample string", "string", 2));

    }
}
