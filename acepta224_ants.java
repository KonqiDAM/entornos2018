import java.util.Scanner;
public class acepta224_ants
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        
        int cases = sc.nextInt();
        while(cases != 0)
        {
            int position = - 1;
            boolean found = false;
            int[] numbers = new int[cases];
            for (int i = 0; i < cases; i++)
            {
                numbers[i] = sc.nextInt();
            }
            int sum = 0;
            for (int j = cases-1; j >= 0; j--)
            {
                if(sum == numbers[j])
                {
                    found = true;
                    if( position == -1 || numbers[j] > numbers[position])
                        position = j;
                }
                sum += numbers[j];
            }
            
            if(found)
                System.out.println("SI " + (position+1));
            else
                System.out.println("NO");
            
            
            
            cases = sc.nextInt();
        }
        
    }
}
