//Ivan Lazcano Sindin
#include <iostream>
using namespace std;

int main()
{
    int a, b, c, count, total = 0;
    cin >> count;
    while(count--)
    {
        cin >> a;
        cin >> b;
        cin >> c;
        total = 0;
        while(a--)
        {
            total += b;
            b -= c;
        }
        cout << total << endl;
    }
    return 0;
}
