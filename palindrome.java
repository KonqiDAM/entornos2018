import java.util.Scanner;
public class palindrome
{
    public static boolean isPalindrome(String text)
    {
        String s = text.replaceAll(" ", "").toUpperCase();
        for (int i = 0, j = s.length()-1 ; i < s.length()/2; i++, j--)
        {
            if(s.charAt(i) != s.charAt(j))
                return false;
            
        }
        return true;
    }

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.println(isPalindrome("Hannah"));
        System.out.println(isPalindrome("Too hot to hoot"));
        System.out.println(isPalindrome("Java is the best language"));
    }
}
