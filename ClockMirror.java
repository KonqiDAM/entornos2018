import java.util.Scanner;
public class ClockMirror
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        sc.nextLine();
        
        while(count-- != 0)
        {
            String[] line = sc.nextLine().split(":");
            int hour = Integer.parseInt(line[0]);
            int min = Integer.parseInt(line[1]);
            
            min = 60 - min;
            if(min == 60)
                min = 0;
                
            hour = 12 - hour;
            
            if (min != 0)
                hour--;
            if(hour == - 1)
                hour = 11;
            if (hour == 0)
                hour = 12;
                
            System.out.printf("%02d:%02d\n", hour, min);
        }
    }
}
