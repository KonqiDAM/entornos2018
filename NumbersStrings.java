import java.util.Scanner;
public class NumbersStrings
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String[] inputs = new String[4];
        
        for (int i = 0; i < 4; i++)
        {
            inputs[i] = sc.nextLine(); 
        }
        int number1 =  Integer.parseInt(inputs[0] + inputs[1]);
        int number2 =  Integer.parseInt(inputs[2] + inputs[3]);
        
        System.out.printf(number1 + " " + number2);
        System.out.printf("\n%d\n", number1 + number2);
    }
}
