import java.util.Scanner;
public class SecretSanta
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String aux;
        boolean nameFound;
        
        String[] names = sc.nextLine().split(",");
        
        for (int i = 0; i < names.length; i++)
        {
            names[i] = names[i].trim();
        }
        
        for (int i = 0; i < names.length-1; i++)
        {
            for (int j = i+1; j < names.length; j++)
            {
                if(names[i].compareTo(names[j]) > 0)
                {
                    aux = names[i];
                    names[i] = names[j];
                    names[j] = aux;
                }
            }
            
        }
        
        System.out.printf("Give me a name: ");
        aux = sc.nextLine().toLowerCase();
        while(!aux.equals(""))
        {
            nameFound = false;
            
            for (int i = 0; i < names.length; i++)
            {
                if(names[i].toLowerCase().equals(aux))
                {
                    System.out.printf("%s, you Secret Santa is %s\n", 
                                    names[i],
                                    i == names.length - 1 ? names[0] 
                                    : names[i + 1]);
                    nameFound = true;
                }
            }
            
            if(!nameFound)
                System.out.printf("Unknow name!\n");
            
            System.out.printf("Give me a name: ");
            aux = sc.nextLine().toLowerCase();
        }
        System.out.printf("Bye bye!\n");
    }
}
