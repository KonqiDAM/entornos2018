
import java.io.*;
import java.util.*;
import java.time.*;

public class Main4 {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        PrintWriter printWriter = null;

        ArrayList<String> s = new ArrayList<>();
        do {
            System.out.println("Enter a text until white space");
            s.add(sc.nextLine());
        } while (!s.get(s.size() - 1).equals(""));

        try {
            printWriter = new PrintWriter(new BufferedWriter(
                    new FileWriter("annotations.txt", true)));
            for (int i = 0; i < s.size()-1; i++) {
                printWriter.println(s.get(i) + " " + LocalDateTime.now());

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

}
