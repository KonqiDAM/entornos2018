#include <iostream>
#include <stdlib.h>
using namespace std;
int main()
{
    
    long long rows, impact;
    long long bowls, bowls2;
    long long result;
    do
    {
        cin >> rows;
        cin >> impact;
        
        if (rows == 0 && impact == 0)
            return 0;
        
        bowls = rows * (rows+1) / 2;
        bowls2 = (rows - impact + 1) * (rows - impact + 2) / 2;
        
        result = bowls - bowls2;
        cout << result << endl;
    }while(true);
}
