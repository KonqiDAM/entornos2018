#include <iostream>
using namespace std;

int main()
{
    int n;
    int countLetter;
    string input, text;
    bool found;
    
    scanf("%d", &n);
    getline(std::cin, input);
    while(n--)
    {
        getline(std::cin, input);
        getline(std::cin, text);
        countLetter = 0;
        found = false;
        
        for ( int i = 0; i < (int) input.length() ; i++)
        {
            if(tolower(text[countLetter]) == tolower(input[i]))
            {
                if(countLetter == (int)text.size() - 1)
                {
                    found = true;
                    break;
                }
                countLetter++;
                while(text[countLetter] == ' ')
                {
                    countLetter++;
                }
            }
        }
        printf("%s", found ? "SI\n" : "NO\n");
    }
    return 0x0;
}
