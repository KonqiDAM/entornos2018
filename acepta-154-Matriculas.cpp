#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
    int number;
    char firstLetter, secondsLetter, thirdLetter;
    do
    {
        cin >> number >> thirdLetter  >> secondsLetter  >> firstLetter ;
        if(number == 9999 && firstLetter == 'Z' 
                        && secondsLetter == 'Z' && thirdLetter == 'Z')
            break;
        
        if(number < 9999)
            number++;
        else
        {
            number = 0;
            firstLetter++;
            if( firstLetter == 'E' || firstLetter == 'I'
                     || firstLetter == 'O' || firstLetter == 'U')
                      firstLetter++;
            if(firstLetter == '[')//If it pases Z restart it
            {
                firstLetter = 'B';
                secondsLetter++;
                if( secondsLetter == 'E' || secondsLetter == 'I'
                     || secondsLetter == 'O' || secondsLetter == 'U')
                      secondsLetter++;
                
            }
            if(secondsLetter == '[')
            {
                secondsLetter = 'B';
                thirdLetter++;
                if( thirdLetter == 'E' || thirdLetter == 'I'
                     || thirdLetter == 'O' || thirdLetter == 'U')
                      thirdLetter++;
            }
        }
        printf("%04d %c%c%c\n", number, thirdLetter, 
                            secondsLetter, 
                            firstLetter);
    }while(true);
    return 0x0;
}
