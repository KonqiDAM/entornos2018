import java.util.Scanner;
import java.util.Arrays; 

public class acepta100
{
    public static void reverseArray(char[] arr)
    {
        char[] aux = new char[4];
        for (int i = 0; i < arr.length; i++)
        {
            aux[i] = arr[arr.length - 1 - i];
            
        }
        for (int i = 0; i < 4; i++)
        {
            arr[i] = aux[i];
        }
    }
    
    public static boolean isRepdigits(int n)
    {
        if (n == 0)
            return true;
        String sn = Integer.toString(n);
        for (int i = 0; i < sn.length()-1; i++)
        {
            if(sn.charAt(i) != sn.charAt(i+1))
                return false;
        }
        
        return true;
    }
    
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        
        int count = sc.nextInt();
        int steps = 0;
        while(count-- != 0)
        {
            int n1 = sc.nextInt();
            if(n1 == 0)
            {
                System.out.println("8");
                continue;
            }
            while(n1 < 1000)
                        n1 *= 10;
            if( isRepdigits(n1))
                System.out.println("8");
            else if(n1 == 6174)
                System.out.println("0");
            else
            {
                steps = 0;
                do
                {
                    while(n1 < 1000)
                        n1 *= 10;
                    char[] n1chars = ("" + n1).toCharArray();
                    Arrays.sort(n1chars);
                    n1 = Integer.parseInt(String.valueOf(n1chars));
                    reverseArray(n1chars);
                    int n2 = Integer.parseInt(String.valueOf(n1chars));
                    n1 = n2 - n1;
                    steps++;
                }while(n1 != 6174 && steps < 8);
                System.out.println(steps);
            }
        }
    }
}
