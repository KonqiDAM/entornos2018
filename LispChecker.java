import java.util.Scanner;
public class LispChecker
{
    public static void main(String[] args)
    {
        int countOP = 0, countCP = 0;
        Scanner sc = new Scanner(System.in);
        boolean wrongOrder = false;
        
        System.out.println("Enter lisp instruction: ");
        String input = sc.nextLine();
        
        for (int i = 0; i < input.length(); i++)
        {
            if(input.charAt(i) == '(')
                countOP++;
            else if(input.charAt(i) == ')')
                countCP++;
            if(countCP > countOP)
                wrongOrder = true;
        }
        if(countOP == countCP && !(wrongOrder))
            System.out.println("Correct");
        else
            System.out.println("Wrong");
    }
}
