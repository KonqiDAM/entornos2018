import java.util.Scanner;
import java.util.Arrays; 

public class SortJoin
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.printf("Enter names in a line (separated with spaces): ");
        String[] names = sc.nextLine().trim().split(" ");
        for (int i = 0; i < names.length-1; i++)
        {
            for (int j = i+1; j < names.length; j++)
            {
                if(names[i].compareTo(names[j]) > 0)
                {
                    String aux = names[i];
                    names[i] = names[j];
                    names[j] = aux;
                }
            }
            
        }
        Arrays.sort(names);
        String output = String.join(", ", names);
        System.out.println(output);
        for (int i = 0; i < names.length-1; i++)
        {
            System.out.printf("%s, ", names[i]);
        }
        System.out.printf("%s", names[names.length-1]);
    }
}
