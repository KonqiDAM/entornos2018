import java.util.Scanner;
public class Factorial
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int total;
        while(num-- != 0)
        {
            int n = sc.nextInt();
            if (n == 0) System.out.println(1);
            else if (n == 3)  System.out.println(6);
            else if (n < 5) System.out.println(n);
            else System.out.println(0);
        }
        
    }
}
