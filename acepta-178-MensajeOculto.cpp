#include <iostream>
using namespace std;

int main()
{
    int n;
    long unsigned int countLetter;
    string entrada, mensaje;
    bool found;
    
    cin >> n;
    getline(std::cin, entrada);
    while(n--)
    {
        getline(std::cin, entrada);
        getline(std::cin, mensaje);
        countLetter = 0;
        found = false;
        
        for ( int i = 0; i < (int) entrada.length() ; i++)
        {
            if(tolower(mensaje[countLetter]) == tolower(entrada[i]))
            {
                if(countLetter == mensaje.size() - 1)
                {
                    found = true;
                    break;
                }
                countLetter++;
                while(mensaje[countLetter] == ' ')
                {
                    countLetter++;
                }
            }
        }
        if(found)
            cout << "SI" << endl;
        else
            cout << "NO" << endl;
    }
    return 0x0;
}
