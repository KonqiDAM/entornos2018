//Ivan Lazcano Sindin
#include <stdio.h>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int a, b, count;
    scanint(count);
    while(count--)
    {
        scanint(a);
        scanint(b);
        fwrite((a == 25 && b == 12 ? "SI\n" : "NO\n"), 1, 3, stdout);
    }
    
}
