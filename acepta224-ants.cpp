#include <iostream>
#include <cstdio>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int count;
    scanint(count);
    while(count)
    {
        int position = - 1;
        bool found = false;
        int numbers[count];
        for (int i = 0; i < count; i++)
        {
            scanint(numbers[i]);
        }
        int sum = 0;
        for (int j = count-1; j >= 0; j--)
        {
            if(sum == numbers[j])
            {
                found = true;
                if( position == -1 || numbers[j] > numbers[position])
                    position = j;
            }
            sum += numbers[j];
        }
        
        if(found)
            printf("SI %d\n", position+1);
        else
            printf("NO\n");        
        scanint(count);
    }
    return 0x0;
}
