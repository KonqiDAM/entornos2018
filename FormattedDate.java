import java.util.Scanner;
public class FormattedDate
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.printf("Enter your birth date, the day: ");
        int day = sc.nextInt();
        System.out.printf("The month: ");
        int month = sc.nextInt();
        System.out.printf("And the year: ");
        int year = sc.nextInt();
        sc.close();
        
        System.out.printf("The number is %d/%d/%d", day, month, year);
    }
}
